import React from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';

import {Note} from './Note';

const Sound = require('react-native-sound');
Sound.setCategory('Playback');

const ivory = ['C', 'D', 'E', 'F', 'G', 'A', 'B'];
const ebony = ['C', 'D', 'F', 'G', 'A'];

const isPortrait = () => {
  const dim = Dimensions.get('screen');
  return dim.height >= dim.width;
};

export default class App extends React.Component {

  constructor(props){
    super(props)
    this.state = {
        orientation: isPortrait() ? 'portrait' : 'landscape',
    };
 
    // Event Listener for orientation changes
    Dimensions.addEventListener('change', () => {
        this.setState({
            orientation: isPortrait() ? 'portrait' : 'landscape'
        });
    });

  }

  createSound(note){
    return new Sound(note + '.wav', Sound.MAIN_BUNDLE, (error) => {
      if (error) {
          console.log('failed to load the sound', error, note);
          return;
            }
          }
        )
  }

  render() {
    
    pos = 0
    height = 80
    octaves = 2
    octavePos = 0
    ivoryKeys = []
    ebonyKeys = []

    keyWidth = Dimensions.get('window').width/octaves/ivory.length
    for(octave = 0; octave < octaves ; octave++){
      
      pos = octavePos;
      octaveNotation = octave + 1; 

      //Ivory octave
      pianoNote=null;
      ivoryKeys.push(ivory.map( note =>{

        the_note = note + octaveNotation;
        let play_note = this.createSound(the_note)

        pianoNote = <Note position={pos} width = {keyWidth} key={the_note} name={the_note} 
        play_note={play_note}
        top = {Dimensions.get('window').height/2 - height/2}
        height = {height}
        />
        pos += keyWidth;
        console.log('creating ', the_note);
        return pianoNote;
      } ) )

      pos = octavePos;

      //Ebony octave
      ebonyKeys.push(ivory.map( note => {

          pianoNote = null;
          the_note = note + octaveNotation;
          if(ebony.indexOf(note)!=-1){
          let play_note = this.createSound(the_note + 's')
          
          pianoNote = <Note position={pos} width = {keyWidth} key={the_note + "s"} name={the_note + "s"} 
          play_note = {play_note}
          top = {Dimensions.get('window').height/2 - height/2}
          height = {height/1.8}
          />
          console.log('creating ', the_note + "s");
        }
        pos += keyWidth;
        return pianoNote;
      } ))
      
      pos = 0
      
      octavePos += keyWidth * ivory.length;  
  }

    return (
      <View style={styles.container}>
        <View style={styles.keyboard}>
          {ivoryKeys}
          {ebonyKeys}
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    backgroundColor: '#fff',
    alignItems: 'center',
    
  },
  keyboard: {
    position: 'absolute',
  },
});
  

  

