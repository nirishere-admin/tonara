Installation:
=============

1. clone with 
   git clone https://nirishere-admin@bitbucket.org/nirishere-admin/tonara.git
2. cd tonara
3. npm install
4. npm start
5. open the ios/piano.xcodeproj with xcode
6. run either on device or simulator
