'use strict';
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions,
} from 'react-native';


var styles = StyleSheet.create({
    ivory: {
        position: 'absolute',
        backgroundColor: 'white',
        borderRadius: 4,
        borderWidth: 1,
        borderColor: 'black',
      },
      ebony: {
        position: 'absolute',
        backgroundColor: 'black',
        borderRadius: 4,
        borderColor: 'black',
      }
});


export class Note extends Component {
  constructor(props){
    super(props);
    this.playNote = this.playNote.bind(this);

  };

  playNote(){
    
        this.props.play_note.play((success) => {
          if (success) {
            console.log('successfully finished playing');
          } else {
            console.log('playback failed due to audio decoding errors');
            this.props.play_note.reset();
          }
        });
    
  }

  render (){

    var res
    //Major note?
    if(this.props.name.indexOf('s')==-1)
      res = <View style={[styles.ivory, {left: this.props.position, width: this.props.width, top:this.props.top, height:this.props.height}]}>
      </View>
    else
      res = <View style={[styles.ebony, {left: this.props.position + 3 * this.props.width /4 , width: this.props.width/2, top:this.props.top, height:this.props.height}]}>
      </View>

    return(
    <View>
        <TouchableOpacity onPress={()=>{this.playNote(this.props.name)}}>
        {res}
      </TouchableOpacity>
    </View>);
  }

}